using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Reflection;

namespace RADSense.Windows.Forms.Design
{
	/// <summary>
	/// Summary description for TabPageConverter.
	/// </summary>
	public class TabPageConverter : TypeConverter
	{
	
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) {

			if (destinationType == typeof(InstanceDescriptor))
				return true;

			return base.CanConvertTo (context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType) {

			if ((destinationType == typeof(InstanceDescriptor)) && (value is TabPage)) {
				
				TabPage page = (TabPage) value;
			
				ConstructorInfo ctor = typeof(TabPage).GetConstructor(new Type[] {});
				if (ctor != null) {
					return new InstanceDescriptor(ctor, new object[] {}, false);
				}
			}
			return base.ConvertTo (context, culture, value, destinationType);
		}

	}
}
