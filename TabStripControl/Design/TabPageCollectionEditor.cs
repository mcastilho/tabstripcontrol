using System;
using System.ComponentModel.Design;
using System.Windows.Forms;

namespace RADSense.Windows.Forms.Design
{
	/// <summary>
	/// Summary description for TabPageCollectionEditor.
	/// </summary>
	public class TabPageCollectionEditor : CollectionEditor
	{

		public TabPageCollectionEditor(Type type) : base(type) {
		}

		/// <summary>
		/// Gets the data types that this collection editor can contain.
		/// </summary>
		/// <returns>An array of data types that this collection can contain.</returns>
		protected override Type[] CreateNewItemTypes() {
			return new Type[] {typeof(TabPage)};
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="editValue"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		protected override object SetItems(object editValue, object[] value) {

			object obj = base.SetItems(editValue, value);

			this.Control.CalculateMetrics();
			this.Control.Invalidate();

			return obj;
		}
		

		protected override void DestroyInstance(object instance) {

			base.DestroyInstance(instance);

			this.Control.CalculateMetrics();
			this.Control.Invalidate();
		}

		protected TabStripControl Control {
			get {
				return (TabStripControl) this.Context.Instance;
			}
		}


	}
}
