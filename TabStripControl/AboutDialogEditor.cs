#region Copyright (c) 2002-2005 RADSense Software Inc. 
//------------------------------------------------------------------------------
// Copyright (c) 2002-2005 RADSense Software Inc.                               
// All Rights Reserved.                                                         
//                                                                              
//                                                                              
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.                     
//                                                                              
// THE CONTENTS OF THIS FILE ARE PROTECTED BY THE UNITED STATES AND             
// INTERNATIONAL COPYRIGHT LAWS. UNAUTHORIZED REPRODUCTION AND/OR               
// DISTRIBUTION  OF ALL OR ANY PORTION OF THE CODE CONTAINED HEREIN             
// IS STRICTLY PROHIBITED AND WILL RESULT IN SEVERE CIVIL AND CRIMINAL          
// PENALTIES. ANY VIOLATIONS OF THIS COPYRIGHT WILL BE PROSECUTED TO            
// THE FULLEST EXTENT POSSIBLE UNDER LAW.                                       
//                                                                              
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY               
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT                  
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR                  
// FITNESS FOR A PARTICULAR PURPOSE.                                            
//                                                                              
// UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN            
// PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR         
// SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY RADSENSE PRODUCT.               
//                                                                              
//------------------------------------------------------------------------------
#endregion

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Runtime.InteropServices;

namespace RADSense.Shared
{

	/// <summary>
	/// The propertry editor for the '(About)' property
	/// </summary>
	[
	  ComVisible(false),
	  EditorBrowsable(EditorBrowsableState.Never)
	]
	public sealed class AboutDialogEditor : UITypeEditor {


		/// <summary>
		/// Gets the editor style used by the <see cref="System.Drawing.Design.UITypeEditor.EditValue"/> method
		/// </summary>
		/// <param name="context">
		/// An ITypeDescriptorContext that can be used to gain additional context information.
		/// </param>
		/// <returns>
		/// A UITypeEditorEditStyle value that indicates the style of editor used by EditValue. 
		/// If the UITypeEditor does not support this method, then GetEditStyle will return None.
		/// </returns>
		public override UITypeEditorEditStyle GetEditStyle ( ITypeDescriptorContext context ) {
			return UITypeEditorEditStyle.Modal;
		}

		/// <summary>
		/// Edits the specified object's value using the editor style indicated by GetEditStyle.
		/// </summary>
		/// <param name="context">
		/// An ITypeDescriptorContext that can be used to gain additional context information.
		/// </param>
		/// <param name="provider">
		/// An IServiceProvider that this editor can use to obtain services.
		/// </param>
		/// <param name="value">
		/// The object to edit.
		/// </param>
		/// <returns>
		/// The new value of the object.
		/// </returns>
		public override object EditValue ( ITypeDescriptorContext context , IServiceProvider provider , object value ) {

			//TODO: get license interface from control ILicensedComponent and 
			//      pass that to the dialog to retrieve information to be 
			//      displayed inside the dialog.

			using (AboutDialog about = new AboutDialog()) {
				about.ShowDialog();	
			}

			return null;
		}
	}

}
