#region Copyright (c) 2002-2005 RADSense Software Inc. 
//------------------------------------------------------------------------------
// Copyright (c) 2002-2005 RADSense Software Inc.                               
// All Rights Reserved.                                                         
//                                                                              
//                                                                              
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.                     
//                                                                              
// THE CONTENTS OF THIS FILE ARE PROTECTED BY THE UNITED STATES AND             
// INTERNATIONAL COPYRIGHT LAWS. UNAUTHORIZED REPRODUCTION AND/OR               
// DISTRIBUTION  OF ALL OR ANY PORTION OF THE CODE CONTAINED HEREIN             
// IS STRICTLY PROHIBITED AND WILL RESULT IN SEVERE CIVIL AND CRIMINAL          
// PENALTIES. ANY VIOLATIONS OF THIS COPYRIGHT WILL BE PROSECUTED TO            
// THE FULLEST EXTENT POSSIBLE UNDER LAW.                                       
//                                                                              
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY               
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT                  
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR                  
// FITNESS FOR A PARTICULAR PURPOSE.                                            
//                                                                              
// UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN            
// PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR         
// SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY RADSENSE PRODUCT.               
//                                                                              
//------------------------------------------------------------------------------
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Windows.Forms;
using System.Runtime.Serialization;

namespace RADSense.Windows.Forms
{

	/// <summary>
	/// 
	/// </summary>
	public class TabPageCollection: CollectionBase
	{

		#region private fields...
		private readonly TabStripControl _Owner = null;
		#endregion

		#region constructors...
		
		public TabPageCollection(TabStripControl owner) {
			_Owner = owner;
		}
		#endregion

		#region protected methods...
		protected override void OnClearComplete() {
			base.OnClearComplete ();

			_Owner.SelectedTabPage = null;
			_Owner.CalculateMetrics();
			_Owner.Invalidate();
		}


		protected override void OnInsertComplete(int index, object value) {
			base.OnInsertComplete (index, value);
			_Owner.CalculateMetrics();
			_Owner.Invalidate();
		}

		protected override void OnRemoveComplete(int index, object value) {
			base.OnRemoveComplete (index, value);
			_Owner.CalculateMetrics();
			_Owner.Invalidate();
		}

//		protected override void OnSetComplete(int index, object oldValue, object newValue) {
//			base.OnSetComplete (index, oldValue, newValue);
//			_Owner.CalculateMetrics();
//			_Owner.Invalidate();
//		}
		#endregion

		#region public methods...
		// public methods...

		public int Add(TabPage tabPage) {
			int index = List.Add(tabPage);
			return index;
		}
		
		
		public int IndexOf(TabPage tabPage) {
			for(int i = 0; i < List.Count; i++)
				if (this[i] == tabPage)    // Found it
					return i;
			return -1;
		}
		
		
		public void Insert(int index, TabPage tabPage) {
			List.Insert(index, tabPage);
		}
		
		
		public void Remove(TabPage tabPage) {
			List.Remove(tabPage);
		}
		
		
		// TODO: If desired, change parameters to Find method to search based on a property of TabPage.
		public TabPage Find(TabPage tabPage) {
			foreach(TabPage lTabPage in this)
				if (lTabPage == tabPage)    // Found it
					return lTabPage;
			return null;    // Not found
		}
		
		
		// TODO: If you changed the parameters to Find (above), change them here as well.
		public bool Contains(TabPage tabPage) {
			return (Find(tabPage) != null);
		}
		
		
		#endregion
 	
		#region public properties...
		
		// public properties...
		
		public TabPage this[int index] {
			get {
				return (TabPage) base.List[index];
			}
			set {
				List[index] = value;
			}
		}
		
		#endregion
		
	}


}
