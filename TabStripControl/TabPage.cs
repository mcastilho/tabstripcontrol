#region Copyright (c) 2002-2005 RADSense Software Inc. 
//------------------------------------------------------------------------------
// Copyright (c) 2002-2005 RADSense Software Inc.                               
// All Rights Reserved.                                                         
//                                                                              
//                                                                              
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.                     
//                                                                              
// THE CONTENTS OF THIS FILE ARE PROTECTED BY THE UNITED STATES AND             
// INTERNATIONAL COPYRIGHT LAWS. UNAUTHORIZED REPRODUCTION AND/OR               
// DISTRIBUTION  OF ALL OR ANY PORTION OF THE CODE CONTAINED HEREIN             
// IS STRICTLY PROHIBITED AND WILL RESULT IN SEVERE CIVIL AND CRIMINAL          
// PENALTIES. ANY VIOLATIONS OF THIS COPYRIGHT WILL BE PROSECUTED TO            
// THE FULLEST EXTENT POSSIBLE UNDER LAW.                                       
//                                                                              
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY               
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT                  
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR                  
// FITNESS FOR A PARTICULAR PURPOSE.                                            
//                                                                              
// UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN            
// PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR         
// SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY RADSENSE PRODUCT.               
//                                                                              
//------------------------------------------------------------------------------
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Windows.Forms;
using System.Runtime.Serialization;

using RADSense.Windows.Forms.Design;

namespace RADSense.Windows.Forms
{

	/// <summary>
	/// 
	/// </summary>
	[
	ToolboxItem(false),
	Serializable,
	TypeConverter(typeof(TabPageConverter))
	]
	public class TabPage  
	{

		#region private fields...
		private string _Text;
		private bool _Visible = true;
		private bool _Enabled = true;

		private Rectangle _TabRectangle = Rectangle.Empty;
		#endregion
		
		#region constructors...
		public TabPage() {
			
		}
		
		#endregion

		#region public properties...
		
		public bool Enabled {
			get {
				return _Enabled;
			}
			set {
				if (_Enabled == value)
					return;
				_Enabled = value;
			}
		}
		
		public string Text {
			get {
				return _Text;
			}
			set {
				if (_Text == value)
					return;
				_Text = value;
			}
		}

		public bool Visible {
			get {
				return _Visible;
			}
			set {
				if (_Visible == value)
					return;
				_Visible = value;
			}
		}


		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Rectangle TabRectangle {
			get {
				return _TabRectangle;
			}
			set {
				if (_TabRectangle == value)
					return;
				_TabRectangle = value;
			}
		}


		#endregion

		
	}

}
