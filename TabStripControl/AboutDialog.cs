#region Copyright (c) 2002-2005 RADSense Software Inc. 
//------------------------------------------------------------------------------
// Copyright (c) 2002-2005 RADSense Software Inc.                               
// All Rights Reserved.                                                         
//                                                                              
//                                                                              
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.                     
//                                                                              
// THE CONTENTS OF THIS FILE ARE PROTECTED BY THE UNITED STATES AND             
// INTERNATIONAL COPYRIGHT LAWS. UNAUTHORIZED REPRODUCTION AND/OR               
// DISTRIBUTION  OF ALL OR ANY PORTION OF THE CODE CONTAINED HEREIN             
// IS STRICTLY PROHIBITED AND WILL RESULT IN SEVERE CIVIL AND CRIMINAL          
// PENALTIES. ANY VIOLATIONS OF THIS COPYRIGHT WILL BE PROSECUTED TO            
// THE FULLEST EXTENT POSSIBLE UNDER LAW.                                       
//                                                                              
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY               
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT                  
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR                  
// FITNESS FOR A PARTICULAR PURPOSE.                                            
//                                                                              
// UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN            
// PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR         
// SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY RADSENSE PRODUCT.               
//                                                                              
//------------------------------------------------------------------------------
#endregion

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace RADSense.Shared
{
	/// <summary>
	/// Summary description for About.
	/// </summary>
	public class AboutDialog : System.Windows.Forms.Form
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Panel panelHeader;
		private System.Windows.Forms.Label lblComponentName;
		private System.Windows.Forms.Label lblProductName;
		private System.Windows.Forms.PictureBox pictureBox1;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AboutDialog));
			this.btnOK = new System.Windows.Forms.Button();
			this.panelHeader = new System.Windows.Forms.Panel();
			this.lblComponentName = new System.Windows.Forms.Label();
			this.lblProductName = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panelHeader.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(469, 259);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 0;
			this.btnOK.Text = "OK";
			// 
			// panelHeader
			// 
			this.panelHeader.BackColor = System.Drawing.Color.White;
			this.panelHeader.Controls.Add(this.lblComponentName);
			this.panelHeader.Controls.Add(this.lblProductName);
			this.panelHeader.Controls.Add(this.pictureBox1);
			this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelHeader.Location = new System.Drawing.Point(0, 0);
			this.panelHeader.Name = "panelHeader";
			this.panelHeader.Size = new System.Drawing.Size(561, 65);
			this.panelHeader.TabIndex = 1;
			// 
			// lblComponentName
			// 
			this.lblComponentName.BackColor = System.Drawing.Color.Transparent;
			this.lblComponentName.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblComponentName.Location = new System.Drawing.Point(29, 37);
			this.lblComponentName.Name = "lblComponentName";
			this.lblComponentName.Size = new System.Drawing.Size(355, 21);
			this.lblComponentName.TabIndex = 2;
			this.lblComponentName.Text = "TabStripControl";
			this.lblComponentName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// lblProductName
			// 
			this.lblProductName.BackColor = System.Drawing.Color.Transparent;
			this.lblProductName.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
			this.lblProductName.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblProductName.Location = new System.Drawing.Point(9, 4);
			this.lblProductName.Name = "lblProductName";
			this.lblProductName.Size = new System.Drawing.Size(375, 29);
			this.lblProductName.TabIndex = 1;
			this.lblProductName.Text = "RADSense";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(408, 7);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(141, 51);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// AboutDialog
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnOK;
			this.ClientSize = new System.Drawing.Size(561, 298);
			this.Controls.Add(this.panelHeader);
			this.Controls.Add(this.btnOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "AboutDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "About";
			this.Load += new System.EventHandler(this.AboutDialog_Load);
			this.panelHeader.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		
		#endregion

		#region private fields...
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		private const int CS_DROPSHADOW = 0x00020000;

		private Timer _FadeInTimer = null;

		#endregion

		#region constructors...
		public AboutDialog() {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		
		#endregion

		#region private methods...
		private void DialogFadeIn() {
			_FadeInTimer = new Timer();
			_FadeInTimer.Interval = 100;
			_FadeInTimer.Tick += new EventHandler(this.OnDialogFadeInTimerTick);
			_FadeInTimer.Enabled = true;
		}

		private void OnDialogFadeInTimerTick(object source, EventArgs e) {
			this.Opacity += .2;

			if (this.Opacity >= 1.0) {
				_FadeInTimer.Enabled = false;
				_FadeInTimer.Tick -= new EventHandler(this.OnDialogFadeInTimerTick);
				_FadeInTimer.Dispose();
			}
		}

		
		#endregion

		#region private events...
		private void AboutDialog_Load(object sender, System.EventArgs e) {
		
			this.Opacity = 0.0;
			this.DialogFadeIn();
		}
		#endregion

		#region protected methods...
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		
		#endregion
		
	}
}
