#region Copyright (c) 2002-2005 RADSense Software Inc. 
//------------------------------------------------------------------------------
// Copyright (c) 2002-2005 RADSense Software Inc.                               
// All Rights Reserved.                                                         
//                                                                              
//                                                                              
// THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.                     
//                                                                              
// THE CONTENTS OF THIS FILE ARE PROTECTED BY THE UNITED STATES AND             
// INTERNATIONAL COPYRIGHT LAWS. UNAUTHORIZED REPRODUCTION AND/OR               
// DISTRIBUTION  OF ALL OR ANY PORTION OF THE CODE CONTAINED HEREIN             
// IS STRICTLY PROHIBITED AND WILL RESULT IN SEVERE CIVIL AND CRIMINAL          
// PENALTIES. ANY VIOLATIONS OF THIS COPYRIGHT WILL BE PROSECUTED TO            
// THE FULLEST EXTENT POSSIBLE UNDER LAW.                                       
//                                                                              
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY               
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT                  
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR                  
// FITNESS FOR A PARTICULAR PURPOSE.                                            
//                                                                              
// UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN            
// PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR         
// SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY RADSENSE PRODUCT.               
//                                                                              
//------------------------------------------------------------------------------
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Data;
using System.Windows.Forms;

using RADSense.Windows.Forms.Design;

namespace RADSense.Windows.Forms
{

	/// <summary>
	/// Summary description for ConsoleTabControl.
	/// </summary>
	[ToolboxItem(true)]
	public class TabStripControl : System.Windows.Forms.Control
	{
		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			components = new System.ComponentModel.Container();
		}
		#endregion
		
		#region private fields...
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private TabPageCollection _Tabs;
		private TabPage _SelectedTabPage = null;

		private ImageList _ImageList;

		private int _TabHeight = 18;
		private int _TabPadding = 0;

		private Hashtable _GraphicsPaths = new Hashtable();


		#endregion

		#region constructors...
		public TabStripControl() {
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			_Tabs = new TabPageCollection(this);

			// TODO: Add any initialization after the InitializeComponent call

			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.ResizeRedraw, true);
			SetStyle(ControlStyles.DoubleBuffer, true);

			CalculateMetrics();
		}

		#endregion
		
		#region protected methods...
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		protected override void OnCreateControl() {
			base.OnCreateControl ();
			CalculateMetrics();
		}


		protected override void OnResize(EventArgs e) {
			base.OnResize (e);
			CalculateMetrics();
			Invalidate();
		}


		protected override void OnLostFocus(EventArgs e) {
			base.OnLostFocus (e);

			if (SelectedTabPage == null)
				return;

			Invalidate(SelectedTabPage.TabRectangle);
		}

		protected override void OnGotFocus(EventArgs e) {
			base.OnGotFocus (e);

			if (SelectedTabPage == null)
				return;

			Invalidate(SelectedTabPage.TabRectangle);
		}


		protected override void OnPaint(PaintEventArgs e) {
			base.OnPaint (e);

			Graphics g = e.Graphics;
			Rectangle clientRect = this.ClientRectangle;
			
			using (Brush brush = new LinearGradientBrush(clientRect, Color.FromArgb(142,179,255), Color.WhiteSmoke, LinearGradientMode.Vertical)) {
				g.FillRectangle(brush, clientRect);
			}

			g.DrawLine(Pens.Gray, clientRect.Left, clientRect.Bottom -1, clientRect.Right - 1, clientRect.Bottom - 1);

			if (_Tabs != null) {

				// iterate through all the tabs
				foreach(TabPage page in _Tabs) {
					if (page.Visible) {
						if (e.ClipRectangle.IntersectsWith(page.TabRectangle)) {
							DrawTabPage(g, page);	
						}
					}
				}
			}

		}


		protected override void OnMouseDown(MouseEventArgs e) {

			this.Focus();

			if (e.Button == MouseButtons.Left) {

				GraphicsPath path;

				// check first the selected tab

				if (_SelectedTabPage != null) {
					path = _GraphicsPaths[_SelectedTabPage] as GraphicsPath;
					if (path != null && path.IsVisible(e.X, e.Y))
						return;
				}

				foreach(TabPage page in _Tabs) {
					
					path =  _GraphicsPaths[page] as GraphicsPath;
					if (path != null && path.IsVisible(e.X, e.Y)) {
						this.SelectedTabPage = page;
						break;
					}
				}

				return;
			}

			base.OnMouseDown (e);
		}


		protected override void OnKeyUp(KeyEventArgs e) {
			
			base.OnKeyUp (e);

			TabPage tab = this.SelectedTabPage;
			if (tab == null)
				return;

			int index = _Tabs.IndexOf(tab);

			switch (e.KeyCode) {
				
				case Keys.Left: {
					
					while (index > 0) {
						TabPage nextPage = _Tabs[--index];
						if (nextPage.Visible) {
							this.SelectedTabPage = nextPage;
							break;
						}
					}
					
					break;
				}

				case Keys.Right: {
					
					while (index < _Tabs.Count-1) {
						TabPage nextPage = _Tabs[++index];
						if (nextPage.Visible) {
							this.SelectedTabPage = nextPage;
							break;
						}
					}
					
					break;
				}

				case Keys.Tab: {

					if (e.Modifiers == Keys.Control) {
						
						if (index == _Tabs.Count-1) {
							index = -1;
							while (index < _Tabs.Count-1) {
								TabPage nextPage = _Tabs[++index];
								if (nextPage.Visible) {
									this.SelectedTabPage = nextPage;
									break;
								}
							}
						}
						else {
							while (index < _Tabs.Count-1) {
								TabPage nextPage = _Tabs[++index];
								if (nextPage.Visible) {
									this.SelectedTabPage = nextPage;
									break;
								}
							}
						}
					}
					break;
				}

			}
			
		}

		
		/// <summary>
		/// 
		/// </summary>
		internal virtual void CalculateMetrics() {
			
			_GraphicsPaths.Clear();

			if (_Tabs == null)
				return;

			Rectangle tabRect;
			Rectangle clientRect = this.ClientRectangle;

			int leftPos = clientRect.Left + 5;
			int topPos = clientRect.Bottom - this.TabHeight;
			int height = this.TabHeight;
			int width = 0; 

			
			foreach(TabPage page in _Tabs) {
	
				if (page.Visible) {
					width = this.GetTabWidth(page.Text);

					tabRect = new Rectangle(leftPos, topPos, width, height);
					page.TabRectangle = tabRect;

					leftPos += width - 10;

					GraphicsPath path = new GraphicsPath();
					path.AddLine(tabRect.Left, tabRect.Bottom, tabRect.Left + 14, tabRect.Top + 3);
					path.AddLine(path.GetLastPoint(), new PointF(tabRect.Left + 16, tabRect.Top + 1));
					path.AddLine(path.GetLastPoint(), new PointF(tabRect.Left + 18, tabRect.Top));
					path.AddLine(path.GetLastPoint(), new PointF(tabRect.Right - 5, tabRect.Top));
					path.AddLine(path.GetLastPoint(), new PointF(tabRect.Right - 3, tabRect.Top + 1));
					path.AddLine(path.GetLastPoint(), new PointF(tabRect.Right - 2, tabRect.Top + 2));
					path.AddLine(path.GetLastPoint(), new PointF(tabRect.Right - 1, tabRect.Top + 4));
					path.AddLine(path.GetLastPoint(), new PointF(tabRect.Right - 1, tabRect.Bottom));
					
					// store the GraphicsPath in hashtable withe TabPage as the key

					if (_GraphicsPaths.Contains(page)) {
						_GraphicsPaths.Remove(page);
					}

					_GraphicsPaths.Add(page, path);

				}
			}

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		private int GetTabWidth(string text) {

			using (Graphics g = Graphics.FromHwnd(this.Handle)) {

				using (Font font = new Font("Tahoma", 8)) {
				
					SizeF size = g.MeasureString(text, font);
					return ((int)size.Width) + (_TabPadding * 2);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="graphics"></param>
		/// <param name="page"></param>
		protected virtual void DrawTabPage(Graphics graphics, TabPage page) {

			if (page == null || page.Visible == false)
				return;

			Rectangle clientRect = this.ClientRectangle;
			
			Font font;
			if (this.SelectedTabPage != page) {
				font = new Font("Tahoma", 8);
			}
			else {
				font = new Font("Tahoma", 8, FontStyle.Bold);
			}

			try {

				StringFormat format = new StringFormat(StringFormatFlags.NoWrap);
				format.Alignment = StringAlignment.Center;
				format.LineAlignment = StringAlignment.Center;
	
				Rectangle rect = page.TabRectangle;

				graphics.SmoothingMode = SmoothingMode.AntiAlias;

				Rectangle clipRect;
				if (_Tabs.IndexOf(page) == 0) {
					clipRect = rect;
				}
				else {
					if (this.SelectedTabPage != page) {
						clipRect = new Rectangle(rect.Left + 10, rect.Top, rect.Width - 10, rect.Height);
					}
					else {
						clipRect = new Rectangle(rect.Left, rect.Top, rect.Width, rect.Height);
					}
				}
				
				graphics.SetClip(clipRect);

				GraphicsPath path = _GraphicsPaths[page] as GraphicsPath;

				if (path == null)
					return;
				
				// fill the Tab with proper color
				if (this.SelectedTabPage != page) {
					using (Brush tabBrush = new LinearGradientBrush(clientRect, Color.FromArgb(210,210,210), Color.White, LinearGradientMode.Vertical)) {
						graphics.FillPath(tabBrush, path);
					}
				}
				else {
					graphics.FillPath(Brushes.White, path);
				}

				// draws the bottom separation line
				if (this.SelectedTabPage != page) {
					graphics.DrawLine(Pens.Gray, clipRect.Left, clipRect.Bottom - 1, clipRect.Right - 1, clipRect.Bottom - 1);
				}

				// draws the outline of that Tab
				graphics.DrawPath(Pens.Gray, path);

				// draws the Tab caption text
				Rectangle textRect = new Rectangle(rect.Left + 5, rect.Top, rect.Width, rect.Height);
				graphics.DrawString(page.Text, font, Brushes.Black, textRect, format);
				

				if (this.Focused && (this.SelectedTabPage == page)) {
					SizeF size = graphics.MeasureString(page.Text, font, textRect.Size, format);
					
					size.Width += 2;
					Rectangle focusRect = new Rectangle(textRect.Left + ((textRect.Width - size.ToSize().Width) / 2) + 1, 
													    textRect.Top + ((textRect.Height - size.ToSize().Height) / 2) + 1,
														size.ToSize().Width, size.ToSize().Height);

					ControlPaint.DrawFocusRectangle(graphics, focusRect);
				}

			}
			finally {
				font.Dispose();
			}

		}

		
		#endregion

		#region public methods...
		/// <summary>
		/// This method returns true if the <see cref="RADSense.Windows.Forms.TabStripControl.ImageList"/> 
		/// property is not null.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public bool ShouldSerializeImageList() {
			return null != this._ImageList;
		}
		#endregion

		#region public events...
		
		public event TabPageChangingHandler TabPageChanging;
		public event TabPageChangedHandler TabPageChanged;

		#region OnTabPageChanging
		/// <summary>
		/// Triggers the TabPageChanging event.
		/// </summary>
		public virtual void OnTabPageChanging(TabPageChangingEventArgs ea) {
			if (TabPageChanging != null)
				TabPageChanging(null/*this*/, ea);
		}
		#endregion
		#region OnTabPageChanged
		/// <summary>
		/// Triggers the TabPageChanged event.
		/// </summary>
		public virtual void OnTabPageChanged(TabPageChangedEventArgs ea) {
			if (TabPageChanged != null) 
				TabPageChanged(null/*this*/, ea);
		}
		#endregion
  

		#endregion

		#region public properties...
		
		/// <summary>
		/// Display the about dialog
		/// </summary>
		[ 
		  DesignOnly(true),
		  EditorBrowsable(EditorBrowsableState.Never),
		  ParenthesizePropertyName(true),
		  Editor(typeof(RADSense.Shared.AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor)),
		  DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
		]
		public object About { 
			get { 
				return null; 
			} 
		}


		[
		DefaultValue((string) null),
		MergableProperty(false),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		Editor(typeof(TabPageCollectionEditor), typeof(UITypeEditor))
		]
		public TabPageCollection Tabs {
			get {
				return _Tabs;
			}
		}

		[
		Browsable(false),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
		]
		public TabPage SelectedTabPage {
			get {
				// if a Tab has not been selected, return the first tab
//				if (_SelectedTabPage == null && _Tabs.Count > 0) {
//					_SelectedTabPage = _Tabs[0];
//					return _SelectedTabPage;
//				}
				return _SelectedTabPage;
			}
			set {
				if (_SelectedTabPage == value)
					return;

//				Rectangle oldRect = Rectangle.Empty;
//				if ((_SelectedTabPage != null) && (_SelectedTabPage.TabRectangle != Rectangle.Empty)) {
//					oldRect = _SelectedTabPage.TabRectangle;
//				}
				

				// invoke the TabPageChanging Event 
				TabPageChangingEventArgs ea = new TabPageChangingEventArgs(_SelectedTabPage);
				OnTabPageChanging(ea);

				// if event was canceled by the handler, then return before changing tab
				if (ea.Cancel)
					return;

				_SelectedTabPage = value;
				
//				if (!oldRect.IsEmpty) {
//					Invalidate(oldRect);
//				}
//
//				if (value != null)
//					Invalidate(value.TabRectangle);
//				else
					Invalidate();

				// invoke the TabPageChanged Event
				if (_SelectedTabPage != null)
					OnTabPageChanged(new TabPageChangedEventArgs(_SelectedTabPage));
			}
		}


		[
		Browsable(false),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
		]
		public int SelectedTabIndex {
			get {
				if (_SelectedTabPage == null)
					return -1;

				for (int i = 0; i < _Tabs.Count; i++) {
					if (_Tabs[i] == _SelectedTabPage)
						return i;
				}
					
				return -1;
			}
			set {
				
				if (value >= _Tabs.Count || value < -1) {
					throw new ArgumentOutOfRangeException("value", "An Invalid Tab Index was specified in SelectedTabIndex");
				}

				if (value == -1) {
					_SelectedTabPage = null;
				}
				else {
					_SelectedTabPage = _Tabs[value];
				}
				
				this.Invalidate();
			}
		}

		public ImageList ImageList {
			get {
				return _ImageList;
			}
			set {
				if (_ImageList == value)
					return;

				_ImageList = value;
				this.Invalidate();
			}
		}
		


		public int TabHeight {
			get {
				return _TabHeight;
			}
			set {
				if (_TabHeight == value)
					return;
				_TabHeight = value;
				CalculateMetrics();
				Invalidate();
			}
		}

		public int TabPadding {
			get {
				return _TabPadding;
			}
			set {
				if (_TabPadding == value)
					return;
				_TabPadding = value;
				CalculateMetrics();
				Invalidate();
			}
		}
  
		#endregion

	}


	/// <summary>
	/// Delegate for the TabPageChanging event.
	/// </summary>
	public delegate void TabPageChangingHandler(object sender, TabPageChangingEventArgs ea);
	/// <summary>
	/// Arguments for the TabPageChanging event.
	/// </summary>
	public class TabPageChangingEventArgs: EventArgs {

		#region private fields...
		private bool _Cancel = false;
		private TabPage _Page;
		#endregion

		#region constructors...
		public TabPageChangingEventArgs(TabPage page) {
			_Page = page;
		}
		
		#endregion

		#region public properties...
		public bool Cancel {
			get {
				return _Cancel;
			}
			set {
				if (_Cancel == value)
					return;
				_Cancel = value;
			}
		}
		public TabPage Page {
			get {
				return _Page;
			}
			set {
				if (_Page == value)
					return;
				_Page = value;
			}
		}		
		#endregion
  
	}
	
	/// <summary>
	/// Delegate for the TabPageChanged event.
	/// </summary>
	public delegate void TabPageChangedHandler(object sender, TabPageChangedEventArgs ea);

	/// <summary>
	/// Arguments for the TabPageChanged event.
	/// </summary>
	public class TabPageChangedEventArgs: EventArgs {
		
		#region private fields...
		private TabPage _Page;	
		#endregion

		#region constructors...
		public TabPageChangedEventArgs(TabPage page) {
			_Page = page;
		}
		#endregion

		#region public properties...
		public TabPage Page {
			get {
				return _Page;
			}
			set {
				if (_Page == value)
					return;
				_Page = value;
			}
		}
		#endregion

	}

}

