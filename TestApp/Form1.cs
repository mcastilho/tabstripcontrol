using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace TestApp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private RADSense.Windows.Forms.TabStripControl tabStripControl1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			RADSense.Windows.Forms.TabPage tabPage1 = new RADSense.Windows.Forms.TabPage();
			RADSense.Windows.Forms.TabPage tabPage2 = new RADSense.Windows.Forms.TabPage();
			RADSense.Windows.Forms.TabPage tabPage3 = new RADSense.Windows.Forms.TabPage();
			RADSense.Windows.Forms.TabPage tabPage4 = new RADSense.Windows.Forms.TabPage();
			RADSense.Windows.Forms.TabPage tabPage5 = new RADSense.Windows.Forms.TabPage();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel2 = new System.Windows.Forms.Panel();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.tabStripControl1 = new RADSense.Windows.Forms.TabStripControl();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.Info;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(436, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(185, 388);
			this.panel1.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(11, 46);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(161, 18);
			this.label1.TabIndex = 3;
			this.label1.Text = "General Tasks";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.TabIndex = 0;
			// 
			// splitter1
			// 
			this.splitter1.BackColor = System.Drawing.Color.Silver;
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
			this.splitter1.Location = new System.Drawing.Point(431, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(5, 388);
			this.splitter1.TabIndex = 3;
			this.splitter1.TabStop = false;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.button4);
			this.panel2.Controls.Add(this.button3);
			this.panel2.Controls.Add(this.button2);
			this.panel2.Controls.Add(this.textBox2);
			this.panel2.Controls.Add(this.tabStripControl1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(431, 388);
			this.panel2.TabIndex = 4;
			this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
			// 
			// button2
			// 
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button2.Location = new System.Drawing.Point(84, 194);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(124, 27);
			this.button2.TabIndex = 4;
			this.button2.Text = "button2";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBox2
			// 
			this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox2.Location = new System.Drawing.Point(73, 116);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(77, 20);
			this.textBox2.TabIndex = 3;
			this.textBox2.Text = "";
			// 
			// tabStripControl1
			// 
			this.tabStripControl1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tabStripControl1.Location = new System.Drawing.Point(0, 0);
			this.tabStripControl1.Name = "tabStripControl1";
			this.tabStripControl1.Size = new System.Drawing.Size(431, 27);
			this.tabStripControl1.TabHeight = 20;
			this.tabStripControl1.TabIndex = 2;
			this.tabStripControl1.TabPadding = 20;
			tabPage1.Enabled = true;
			tabPage1.Text = "Summary";
			tabPage1.Visible = true;
			tabPage2.Enabled = true;
			tabPage2.Text = "Settings";
			tabPage2.Visible = true;
			tabPage3.Enabled = true;
			tabPage3.Text = "Rules";
			tabPage3.Visible = true;
			tabPage4.Enabled = true;
			tabPage4.Text = "Members";
			tabPage4.Visible = true;
			tabPage5.Enabled = true;
			tabPage5.Text = "dfgdfg";
			tabPage5.Visible = true;
			this.tabStripControl1.Tabs.Add(tabPage1);
			this.tabStripControl1.Tabs.Add(tabPage2);
			this.tabStripControl1.Tabs.Add(tabPage3);
			this.tabStripControl1.Tabs.Add(tabPage4);
			this.tabStripControl1.Tabs.Add(tabPage5);
			this.tabStripControl1.Text = "tabStripControl1";
			this.tabStripControl1.TabPageChanged += new RADSense.Windows.Forms.TabPageChangedHandler(this.tabStripControl1_TabPageChanged);
			this.tabStripControl1.TabPageChanging += new RADSense.Windows.Forms.TabPageChangingHandler(this.tabStripControl1_TabPageChanging);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(0, 0);
			this.textBox1.Name = "textBox1";
			this.textBox1.TabIndex = 0;
			this.textBox1.Text = "";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(0, 0);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(239, 195);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(83, 27);
			this.button3.TabIndex = 5;
			this.button3.Text = "button3";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(164, 113);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(84, 22);
			this.button4.TabIndex = 6;
			this.button4.Text = "SetIndex";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(621, 388);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e) {
		

			//tabStripControl1.SelectedTabPage = tabStripControl1.Tabs[1];

		}

		private void panel2_Paint(object sender, System.Windows.Forms.PaintEventArgs e) {
		
		}

		private void tabStripControl1_TabPageChanged(object sender, RADSense.Windows.Forms.TabPageChangedEventArgs ea) {
		
			//MessageBox.Show(String.Format("The Tab was changed to: {0}", ea.Page.Text));

		}

		private void tabStripControl1_TabPageChanging(object sender, RADSense.Windows.Forms.TabPageChangingEventArgs ea) {
//		
//			if (MessageBox.Show(this, String.Format("Do you want to save Changes to the {0} section ?", ea.Page.Text), "Save Changes", MessageBoxButtons.YesNoCancel) == DialogResult.Cancel) {
//				ea.Cancel = true;
//			}

		}

		private void button2_Click(object sender, System.EventArgs e) {
		
			tabStripControl1.Tabs.Clear();

		}

		private void button3_Click(object sender, System.EventArgs e) {
		
			RADSense.Windows.Forms.TabPage tab = new RADSense.Windows.Forms.TabPage();
			tab.Text = "Extra";

			tabStripControl1.Tabs.Add(tab);

		}

		private void button4_Click(object sender, System.EventArgs e) {
			
			tabStripControl1.SelectedTabIndex  = Int32.Parse(textBox2.Text);
		}
	}
}
